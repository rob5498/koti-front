export interface Cat {
    nickname: string
    dignity: string
    color: string
    sex: string
    Images?: Array<any>
}