import { Module, VuexModule, getModule, Mutation, Action } from 'vuex-module-decorators'
import store from '@/store'
import { axios } from '@/main'
import { Cat } from './types'

@Module({
  namespaced: true,
  name: 'leadCardSingle',
  store,
  dynamic: true
})
class CatsService extends VuexModule {
  blank_cat: Cat = {
    nickname: '',
    dignity: '',
    color: '',
    sex: ''
  }

  parent_cats: Array<Cat> = []
  for_sale_cats: Array<Cat> = []

  @Action({ rawError: true })
  async saveParentCat(data: any) {
    try {
      let catData: any = new FormData()
      for (let key in data.cat) {
        // @ts-ignore
        catData.append(key, data.cat[key])
      }
      data.images.forEach((image: File) => {
        // @ts-ignore
        catData.append('files', image)
      })
      console.log('catData', catData)
      const response = await axios.post('/parent-cats', catData, {
        // headers: {"Content-Type": "multipart/form-data"}
      })
      return Promise.resolve(response)
    } catch (err) {
      return Promise.reject(err)
    }
  }

  @Mutation
  setCatsList(cats: Array<Cat>) {
    console.log('cats', cats)
    this.parent_cats = cats
  }
  @Mutation
  setForSaleList(cats: Array<Cat>) {
    this.for_sale_cats = cats
  }
  @Action({ rawError: true, commit: 'setCatsList' })
  async getParentCat() {
    try {
      const response = await axios.get('/parent-cats')
      return Promise.resolve(response.data)
    } catch (err) {
      return Promise.reject(err)
    }
  }

  @Action({ rawError: true, commit: 'setForSaleList' })
  async getForSaleCats() {
    try {
      const response = await axios.get('/for-save-cats')
      return Promise.resolve(response.data)
    } catch (err) {
      return Promise.reject(err)
    }
  }
}

export default getModule(CatsService)
