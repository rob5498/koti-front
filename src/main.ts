import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Axios from 'axios'

import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

import VeeValidate from 'vee-validate'
const vvconfig = {
  fieldsBagName: [], // change if property conflicts.
}
Vue.use(VeeValidate, vvconfig)

import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css';
import 'primevue/resources/themes/saga-blue/theme.css'

Vue.config.productionTip = false;

export const axios = Axios.create({
  baseURL: process.env.VUE_APP_STRAPI_DEVELOP,
  timeout: 30000,
  headers: { 'Content-Type': 'application/json' }
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
