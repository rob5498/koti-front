import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/for-sale-cats',
    name: 'For Sale Cats',
    component: () => import('@/views/ForSaleCatsList.vue')
  },
  {
    path: '/lady-cats',
    name: 'Lady Cats',
    component: () => import('@/views/ParentCatsList.vue')
  },
  {
    path: '/male-cats',
    name: 'Male Cats',
    component: () => import('@/views/ParentCatsList.vue')
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: () => import('@/views/Contacts.vue')
  },
  {
    path: '/admin',
    name: 'Admin Panel',
    component: () => import('@/views/AdminPanel.vue'),
    children: [
      {
        path: '/admin/for-sale-cats',
        name: 'Manager For Sale Cats',
        component: () => import('@/components/AdminPanel/ForSaleCats.vue'),
      },
      {
        path: '/admin/parent-cats',
        name: 'Manager Parent Cats',
        component: () => import('@/components/AdminPanel/ParentCats.vue'),
      }
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
