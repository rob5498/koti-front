module.exports = {
    runtimeCompiler: true,
    configureWebpack: {
      module: {
        rules:[
            {
                test: /\.(png|jpg|gif)$/i,
                use: [
                    {
                    loader: 'url-loader',
                    options: {
                        limit: 8192
                    }
                    }
                ]
            }
        ] 
      }
    }
  };
  